import { useState } from "react";

import Dialog from "../components/Dialog";

export default function Page() {
  const [open, setOpen] = useState(true);
  return (
    <div>
      <div>Testing Cronofy elements</div>
      <Dialog open={open} setOpen={setOpen} />
    </div>
  );
}
