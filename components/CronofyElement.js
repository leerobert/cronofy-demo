import React, { useEffect, useState } from "react";
import * as CronofyElements from "cronofy-elements";

const SchedulingElement = ({ options }) => {
  const [element, setElement] = useState(null);

  useEffect(() => {
    if (!element) {
      setElement(
        CronofyElements.SlotPicker({
          target_id: "picker-id",
          ...options,
        })
      );
    }
  }, []);

  useEffect(() => {
    if (element) {
      element.update({ target_id: "picker-id", ...options });
    }
  }, [options]);

  return <div id="picker-id" />;
};

export default SchedulingElement;
